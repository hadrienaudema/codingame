import sys
import math
import os.path

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

n = int(input())  # Number of elements which make up the association table.
q = int(input())  # Number Q of file names to be analyzed.
my_dict = {}
fname=""
fname_sub=""
dot=0
for i in range(n):
    # ext: file extension
    # mt: MIME type.
    ext, mt = input().split()
    my_dict[ext.lower()]=mt
    
for i in range(q):
    
    fname = input().lower()  # One file name per line.
    
    fname_sub=""
    dot=0
    for letter in fname:
        if letter =="." and dot==0:
            dot= dot +1
        elif dot == 1 and letter!=".":
            fname_sub=fname_sub+letter
        elif dot == 1 and letter==".":
            fname_sub=""
    
    if fname_sub  in my_dict and fname_sub[-1:]!=".":
        print(my_dict[fname_sub])
    else:
        print("UNKNOWN")
    
# Write an action using print
# To debug: print("Debug messages...", file=sys.stderr)


# For each of the Q filenames, display on a line the corresponding MIME type. If there is no corresponding type, then display UNKNOWN.
#print("UNKNOWN")