# https://www.codingame.com/ide/puzzle/temperatures
import sys
import math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

n = int(raw_input())  # the number of temperatures to analyse
test=5527
for i in raw_input().split():
    # t: a temperature expressed as an integer ranging from -273 to 5526
    t = int(i)
    if i == 0:
        test=t
    else:
        if abs(test)>abs(t):
            test=t
        elif abs(test)==abs(t):
                if test<=t:
                    test=t
                
   # print  
# Write an action using print
# To debug: print >> sys.stderr, "Debug messages..."

if n == 0:
    print "0"

else:
    print test