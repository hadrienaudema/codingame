# https://www.codingame.com/ide/puzzle/horse-racing-duals

import sys
import math
import bisect 

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

n = int(input())
test = 10000
tampon=0
my_horse = []
for i in range(n):
    pi = int(input())
    bisect.insort(my_horse,pi)

# Write an action using print
# To debug: print("Debug messages...", file=sys.stderr)
for i in range(len(my_horse)):
    if i == 0 :
        tampon=my_horse[i]
    elif i > 0 and abs(my_horse[i]-my_horse[i-1])<test:
        test=abs(my_horse[i]-my_horse[i-1])


print(test)