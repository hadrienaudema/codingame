# https://www.codingame.com/ide/puzzle/defibrillators

import sys
import math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

lon = raw_input()
#print lon
lat = raw_input()
#print lat
n = int(raw_input())
for i in xrange(n):
    defib = raw_input()
    data=defib.split(";")
    
    x = (float(lon.replace(",","."))-float(data[4].replace(",","."))) * math.cos ((float(lat.replace(",",".")) + float(data[5].replace(",",".")))/2)
    y = float(lat.replace(",",".")) - float(data[5].replace(",","."))
    dist = math.sqrt(x**2+y**2)*6371
    
    if i==0:
        test=dist
        location=data[1]
    elif i<>0:
        if dist<=test:
            test=dist
            location=data[1]
  
            
# Write an action using print
# To debug: print >> sys.stderr, "Debug messages..."

print location