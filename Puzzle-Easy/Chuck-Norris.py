# https://www.codingame.com/ide/puzzle/chuck-norris

import sys
import math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

message = raw_input()
message_as_bytes=""
print >> sys.stderr, len(format(ord("%"), 'b'))==6


for i in message:
    if len(format(ord(i), 'b'))==7:
        message_as_bytes=message_as_bytes+format(ord(i), 'b') 
    else:
        message_as_bytes=message_as_bytes+"0"+format(ord(i), 'b') 
# Write an action using print
# To debug: print >> sys.stderr, "Debug messages..."
bit_prec=""
coded_message=""
count=0
test=0

for i in message_as_bytes:
    
    print >> sys.stderr,coded_message
    if count==0:
        
        count+=1
        bit_prec=i
        if i=="0":    
            coded_message=coded_message+"00 "
        elif i=="1":
            coded_message=coded_message+"0 "
    elif count==1:
        
        if bit_prec==i:
            
            coded_message=coded_message+"0"
            bit_prec=i
            
        else:
            bit_prec=i
            if i=="0":    
                coded_message=coded_message+"0"+" 00 "
                count=1
            elif i=="1":
                coded_message=coded_message+"0"+" 0 "
                count=1
            
coded_message=coded_message+"0"            
print coded_message